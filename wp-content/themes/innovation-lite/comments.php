<?php
/* 	Área de comentários de inovação do Lite para páginas únicas
        Copyright: 2015, criação de D5, www.d5creation.com
        Baseado na estrutura de D5 do mais simples para o WordPress
        Desde a inovação Lite 1.0
*/
if ( post_password_required() ) { return; }
?>

<div id="comments">
<?php if ( have_comments() ) : ?>
	<h2 class="commentsbox"><?php comments_number( __('No Comments', 'innovationlite'), __('One Comment', 'innovationlite'),  __('% Comments', 'innovationlite') );?> to  <a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>
	<ol class="commentlist">
		<?php wp_list_comments( array( 'avatar_size' => '200' )  ); ?>
	</ol>
	<div class="comment-nav">
		<div class="floatleft">
			<?php previous_comments_link() ?>
		</div>
		<div class="floatright">
			<?php next_comments_link() ?>
		</div>
	</div>
<?php else : ?>
	<?php if ( ! comments_open() && ! is_page() ) : ?>
		<p class="watermark"><?php echo __('Comments are Closed', 'innovationlite'); ?></p>
	<?php endif; ?>
<?php endif; ?>
<?php // if ( comments_open() ) : ?>
	<!--<div id="comment-form">-->
		<?php // comment_form(); ?>
	<!--</div>-->
<?php // endif; ?>
</div>
