<?php
/**
 * Innovation Lite Options Page
 * @ Copyright: D5 Creation, All Rights, www.d5creation.com
 */

function optionsframework_option_name() {

	// Change this to use your theme slug
	return 'innovationlite';
}
function optionsframework_options() {


// General Options
		$options[] = array(
		'name' => __('Innovation Lite Options', 'innovationlite'), 
		'type' => 'heading');
		
	$options[] = array(
		'desc' => '<div class="infohead"><span class="donation">'. __('A Theme is an effort of many sleepless nights of the Developers.  If you like this FREEE Theme You can consider for a 5 star rating and honest review. Your review will inspire us. You can', 'innovationlite').' <a href="https://wordpress.org/support/view/theme-reviews/innovation-lite" target="_blank"><strong>'. __('Review Here', 'innovationlite').'</strong></a>.</span><br /><br /><br /><span class="donation"> '. __('Need More Features and Options including Multi Layer Slides, Unlimited Slide Items, Links from Featured Boxes, Portfolio and 100+ Advanced Features and Controls? Try', 'innovationlite').' <a href="http://d5creation.com/theme/innovation/" target="_blank"><strong>Innovation Extend</strong></a></span><br /> <br /><br /><span class="donation"> '. __('You can Visit the', 'innovationlite').' Innovation Extend <a href="http://demo.d5creation.com/themes/?theme=Innovation" target="_blank"><strong>DEMO 1</strong></a> and <a href="http://demo.d5creation.com/themes/?theme=Innovation-Box" target="_blank"><strong>DEMO 2</strong></a></span><a href="http://d5creation.com/theme/innovationlite/" target="_blank" class="extendlink"> </a></div>',
		'type' => 'info');
	
	$options[] = array(
		'name' => __('Use Responsive Layout', 'innovationlite'),  
		'desc' => __('Check the Box if you want the Responsive Layout of your Website', 'innovationlite'),  
		'id' => 'responsive',
		'std' => '1',
		'type' => 'checkbox');
		
	foreach (range(1, 5 ) as $numslinksn) {
	$options[] = array(
		'name' => __('Social Link - ',  'innovationlite'). $numslinksn, 
		'desc' => __('Input Your Social Page Link. Example: <b>http://wordpress.org/d5creation</b>.  If you do not want to show anything here leave the box blank. Supported Links are: wordpress.org, wordpress.com, dribbble.com, github.com, tumblr.com, youtube.com, flickr.com, vimeo.com, codepen.io, linkedin.com', 'innovationlite'),  
		'id' => 'sl' . $numslinksn,
		'std' => 'staffpage',
		'type' => 'text');	
	}


	$options[] = array(
		'name' => __('Slide Image 01', 'innovationlite'),  
		'desc' => __('Upload or Input the Slide Image URL Here. Recommended Size: 2300px X 700px', 'innovationlite'),  
		'id' => 'slide-back1',
		'std' => get_template_directory_uri() . '/images/slide/slideback1.jpg',
		'type' => 'upload');
		
	$options[] = array(
		'name' => __('Slide Image 02', 'innovationlite'),  
		'desc' => __('Upload or Input the Slide Image URL Here. Recommended Size: 2300px X 700px', 'innovationlite'),  
		'id' => 'slide-back2',
		'std' => get_template_directory_uri() . '/images/slide/slideback2.jpg',
		'type' => 'upload');
		
	$options[] = array(
		'name' => __('Slide Image 03', 'innovationlite'),  
		'desc' => __('Upload or Input the Slide Image URL Here. Recommended Size: 2300px X 700px', 'innovationlite'),  
		'id' => 'slide-back3',
		'std' => get_template_directory_uri() . '/images/slide/slideback3.jpg',
		'type' => 'upload');
	
	foreach (range(1, 4 ) as $fbsinumber) {
	
	$options[] = array(
		'desc' => '<span class="featured-area-sub-title">'. __('Featured Box: ',  'innovationlite') . $fbsinumber . '</span>', 
		'type' => 'info');
	
	$options[] = array(
		'name' => __('Title', 'innovationlite'),  
		'desc' => __('Input your Featured Title here. Please limit it within 30 Letters. If you do not want to show anything here leave the box blank.', 'innovationlite'),  
		'id' => 'featured-title' . $fbsinumber,
		'std' => __('Innovation Lite Responsive', 'innovationlite'), 
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Description', 'innovationlite'),  
		'desc' => __('Input the description of Featured Area. Please limit the words within 30 so that the layout should be clean and attractive. You can also input any HTML, Videos or iframe here. But Please keep in mind about the limitation of Width of the box.', 'innovationlite'),  
		'id' => 'featured-description' . $fbsinumber,
		'std' => __('The Color changing options of Innovation Lite will give the WordPress Driven Site an attractive look. Innovation Lite is super elegant and Professional Responsive Theme which will create the business widely expressed.', 'innovationlite'), 
		'type' => 'textarea' );

	}
	
	$options[] = array(
		'desc' => '<span class="featured-area-sub-title">---------------</span>', 
		'type' => 'info');

	$options[] = array(
		'name' => __('Portfolio Boxes Heading', 'innovationlite'),  
		'desc' => __('Set the Portfolio Boxes Heading', 'innovationlite'),  
		'id' => 'portfolioboxes-heading',
		'std' => __('A FEW FROM OUR PORTFOLIO', 'innovationlite'), 
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Portfolio Boxes Heading Description', 'innovationlite'),  
		'desc' => __('Set the Portfolio Boxes Heading description', 'innovationlite'),  
		'id' => 'portfolioboxes-heading-des',
		'std' => __('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua', 'innovationlite'), 
		'type' => 'textarea');

	foreach (range(1, 4 ) as $portfolioboxsnumber) {
	$options[] = array(
		'desc' => '<span class="featured-area-sub-title">'. __('Portfolio Box: ',  'innovationlite') . $portfolioboxsnumber . '</span>', 
		'type' => 'info');
	
	$options[] = array(
		'name' => __('Portfolio Image', 'innovationlite'),  
		'desc' => __('Uoload the Portfolio Image. The Sample Image is 400px X 300px', 'innovationlite'),  
		'id' => 'portfolioboxes-image' . $portfolioboxsnumber,
		'std' => get_template_directory_uri() . '/images/pf'.  $portfolioboxsnumber . '.png',
		'type' => 'upload');
	
	$options[] = array(
		'name' => __('Title',  'innovationlite'), 
		'desc' => __('Input your Portfolio Box Title here. Please limit it within 30 Letters. If you do not want to show anything here leave the box blank.',  'innovationlite'), 
		'id' => 'portfolioboxes-title' . $portfolioboxsnumber,
		'std' => __('OUR PROJECT ',  'innovationlite'). $portfolioboxsnumber,
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Description', 'innovationlite'),  
		'desc' => __('Input the description of Portfolio Box. Please limit the words within 30 so that the layout should be clean and attractive.', 'innovationlite'),  
		'id' => 'portfolioboxes-description' . $portfolioboxsnumber,
		'std' => __('Innovation Lite is a Professional Responsive Theme', 'innovationlite'), 
		'type' => 'textarea' );

	}
	
		
	$options[] = array(
		'name' => __('Heading',  'innovationlite'), 
		'desc' => __('Input your heading text here.  Please limit it within 30 Letters.', 'innovationlite'),  
		'id' => 'heading_text1',
		'std' => __('WordPress is web <em>software you can use to create websites!</em> ', 'innovationlite'), 
		'type' => 'text' );
		
	$options[] = array(
		'name' => __('Heading Description',  'innovationlite'), 
		'desc' => __('Input your heading description here. Please limit it within 100 Letters.',  'innovationlite'), 
		'id' => 'heading_des1',
		'std' => __('It is Amazing!  <em>Over 60 million people</em> have chosen WordPress to power the place on the web.', 'innovationlite'), 
		'type' => 'textarea' );
		
	$options[] = array(
		'name' => __('Button URL',  'innovationlite'), 
		'desc' => __('Input the Button URL Here',  'innovationlite'), 
		'id' => 'heading_btn1_link',
		'std' => 'staffpage',
		'type' => 'text' );	
		
	
	$options[] = array(
		'desc' => '<span class="featured-area-sub-title">---------------</span>', 
		'type' => 'info');	

	$options[] = array(
		'name' => __('Staff Boxes Heading', 'innovationlite'),  
		'desc' => __('Set the Staff Boxes Heading',  'innovationlite'), 
		'id' => 'staffboxes-heading',
		'std' => __('WE ARE INSIDE', 'innovationlite'), 
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Staff Boxes Heading Description', 'innovationlite'),  
		'desc' => __('Set the Staff Boxes Heading description', 'innovationlite'),  
		'id' => 'staffboxes-heading-des',
		'std' => __('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua', 'innovationlite'), 
		'type' => 'textarea');	
	
	foreach (range(1, 3 ) as $staffboxsnumber) {
	$options[] = array(
		'desc' => '<span class="featured-area-sub-title">'. __('Staff Box: ',  'innovationlite') . $staffboxsnumber . '</span>', 
		'type' => 'info');
	
	$options[] = array(
		'name' => __('Staff Image',  'innovationlite'), 
		'desc' => __('Uoload the Staff Image. The Sample Image is 300px X 200px',  'innovationlite'), 
		'id' => 'staffboxes-image' . $staffboxsnumber,
		'std' => get_template_directory_uri() . '/images/stf'.  $staffboxsnumber . '.jpg',
		'type' => 'upload');
	
	$options[] = array(
		'name' => __('Name', 'innovationlite'),  
		'desc' => __('Staff Name', 'innovationlite'),  
		'id' => 'staffboxes-title' . $staffboxsnumber,
		'std' => __('OUR PROUD STAFF ',  'innovationlite'). $staffboxsnumber,
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Designation', 'innovationlite'),  
		'desc' => __('Input the Designation', 'innovationlite'),  
		'id' => 'staffboxes-description' . $staffboxsnumber,
		'std' => __('Service Executive', 'innovationlite'), 
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Social Link',  'innovationlite'), 
		'desc' => __('Input the Social Network URL here. Supported Links are: wordpress.org, wordpress.com, dribbble.com, github.com, tumblr.com, youtube.com, flickr.com, vimeo.com, codepen.io, linkedin.com', 'innovationlite'),  
		'id' => 'staffboxes-linka' .$staffboxsnumber,
		'std' => 'staffpage',
		'type' => 'text');	
	
	$options[] = array(
		'desc' => __('Input the Social Network URL here. Supported Links are: wordpress.org, wordpress.com, dribbble.com, github.com, tumblr.com, youtube.com, flickr.com, vimeo.com, codepen.io, linkedin.com',  'innovationlite'), 
		'id' => 'staffboxes-linkb' . $staffboxsnumber,
		'std' => 'staffpage',
		'type' => 'text');	
		
	$options[] = array(
		'desc' => __('Input the Social Network URL here. Supported Links are: wordpress.org, wordpress.com, dribbble.com, github.com, tumblr.com, youtube.com, flickr.com, vimeo.com, codepen.io, linkedin.com',  'innovationlite'), 
		'id' => 'staffboxes-linkc' . $staffboxsnumber,
		'std' => 'https://wordpress.org',
		'type' => 'text');			

	$options[] = array(
		'name' => __('Profile Link',  'innovationlite'), 
		'desc' => __('Input the Staff Profile URL here.',  'innovationlite'), 
		'id' => 'staffboxes-link' . $staffboxsnumber,
		'std' => '#',
		'type' => 'text');
	}

	

	
	return $options;
}

/*
 * This is an example of how to add custom scripts to the options panel.
 * This example shows/hides an option when a checkbox is clicked.
 */

add_action('optionsframework_custom_scripts', 'optionsframework_custom_scripts');

function optionsframework_custom_scripts() { ?>


<?php
}
