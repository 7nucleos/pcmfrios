<?php
/* 	Innovation Lite Theme's Featured Box to show the Featured Items of Front Page
	Copyright: 2015, D5 Creation, www.d5creation.com
	Based on the Simplest D5 Framework for WordPress
	Since Innovation Lite 1.0
*/
?>



<div class="box90">
	<div class="featured-boxs">
            
                                <a href="pcmfrios/sobre/" >
                                    <span class="featured-box" > 
                                            <div class="box-icon fa-rocket"></div>
                                                    <h3 class="ftitle"><?php echo esc_attr(of_get_option('featured-title' . $fboxn, 'Missão')); ?></h3>
                                                    <p><?php echo esc_textarea(of_get_option('featured-description' . $fboxn , 'Prestar serviços de boa qualidade no atendimento aos nossos clientes, visando atende-los sempre com presteza e dedicação, buscando a excelência no relacionamento.')); ?></p>
                                    </span>
                                </a>
                                
                                <a href="pcmfrios/sobre/" >
                                    <span class="featured-box" > 
                                            <div class="box-icon fa-binoculars"></div>
                                                    <h3 class="ftitle"><?php echo esc_attr(of_get_option('featured-title' . $fboxn, 'Visão')); ?></h3>
                                                    <p><?php echo esc_textarea(of_get_option('featured-description' . $fboxn  , 'Ser um grande distribuidor, buscando cada vez mais a eficiência no segmento em que atuamos, objetivando uma prestação de serviço de qualidade e o fornecimento de produtos de marcas reconhecidas.')); ?></p>
                                    </span>
                                </a> 
            
                                <a href="pcmfrios/sobre/" >
                                    <span class="featured-box" > 
                                            <div class="box-icon fa-users"></div>
                                                    <h3 class="ftitle"><?php echo esc_attr(of_get_option('featured-title' . $fboxn , 'Valores')); ?></h3>
                                                    <p><?php echo esc_textarea(of_get_option('featured-description' . $fboxn , 'Nossos objetivos estão pautados na satisfação dos nossos clientes, fornecedores e colaboradores, seguindo uma linha de conduta com base no respeito, honestidade e boas praticas comerciais.')); ?></p>
                                    </span>
                                </a>    
		

	</div> <!-- featured-boxs -->

</div>




<div class="lsep"></div>
