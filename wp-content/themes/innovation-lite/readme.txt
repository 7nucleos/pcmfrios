----------------------------------
Innovation Lite
----------------------------------
Version: 	1.02
Developer: 	D5 Creation
Author URI: 	http://d5creation.com

Donation Link: 	http://d5creation.com/donate/

Copyright: 	D5 Creation
License: 		GNU General Public License v2 or later
License URI: 	http://www.gnu.org/licenses/gpl-2.0.html

This Product is provided "as is" with no waranty or liabilities of D5 Creation. All the PHP Code, Images and other particulars included with this product are licensed under the same License of the Theme.

screenshot.png and
slideback1.jpg		: 	Author - PublicDomainPictures, License - Public Domain CC0,  Source - http://pixabay.com/en/business-card-credit-debit-female-15721/
slideback2.jpg		: 	Author - PublicDomainPictures, License - Public Domain CC0,  Source - http://pixabay.com/en/christmas-claus-cute-gift-girl-15651/
slideback3.jpg		: 	Author - PublicDomainPictures, License - Public Domain CC0,  Source - http://pixabay.com/en/attractive-belly-costume-dance-16042/

stf1.jpg	 		: 	Author - Bergadder, License - Public Domain CC0,  Source - http://pixabay.com/en/girl-young-smile-nice-woman-610544/
stf2.jpg 			: 	Author - Nissor, License - Public Domain CC0,  Source - http://pixabay.com/en/model-fashion-attractive-female-600225/
stf3.jpg			: 	Author - RondellMelling, License - Public Domain CC0,  Source - http://pixabay.com/en/beauty-woman-flowered-hat-cap-354565/

Options Framework	:	Author - Devin Price, License -  GPL-2.0+,  Source - http://wptheming.com
FontAwesome		:	GPL Friendly Licenses, Source -  http://fortawesome.github.io/Font-Awesome
 jQuery FlexSlider		:	License: GPLv2, Copyright: WooThemes,  Author: Tyler Smith, Source: https://github.com/woothemes/FlexSlider
 Modernizr		:	License: BSD and MIT, Copyright (c) Faruk Ates, Paul Irish, Alex Sexton, Source: https://modernizr.com

All other images are Lincenced Under GNU GPL and Copyright to D5 Creation

Limitations: Site Title/ Logo, Top Menu and Main Menu width is fixed. So you should keep design those considering this limitation so that they can fit within the container

Instructions:
- You can set the Slider Images, Portfolio,  Heading, Featured Images, Staff Title, Staff Description and Staff Items from Appearance > Innovation Lite Options



