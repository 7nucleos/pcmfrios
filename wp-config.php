<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'pcm');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/5N|>QdHsq+_7|Mnx==,eY=qXOKFbp0.|4>5oJq>hQ+>OXd)#NC+fm:d9(:*Hj7x');
define('SECURE_AUTH_KEY',  '+)=rU,.*DowT2oL@|p2|3^--$R|o.zR`U!--yUb+zz|#rQ`,Y9NfBV9:oI&(dwg.');
define('LOGGED_IN_KEY',    'b#ELY+&]ty}qI>V+=&p9rkZX5/On|BzZ//)--p5ZVguZ}TGpZKYV|)M6Uw[w;Jog');
define('NONCE_KEY',        'qMH|zj1Zh]<0i4qpYQZQa|Lk@&H=w2[4A[g`!P`xyEE%q;#P^+EkHdtBloPZ?hV:');
define('AUTH_SALT',        '1(D])S6O#ESSu=qB(r+I*~U$@x)-5yy :yTkmHH-HI`za,hIX!5k,:U;++oEomB[');
define('SECURE_AUTH_SALT', '`!2yN3|2)JaOh)oWjWY@4^n`CCOf5nJ%o>]/^|oKv-,x!66| W.noD[tz^mWl(Ze');
define('LOGGED_IN_SALT',   's_6Fs|<hkM,;QXV!*v(1nA`UZK)H9&YRLyNq?}dJ/^j&V68A/mnQg897{}h/SG,H');
define('NONCE_SALT',       'YfRK5aP1nd$,ZK.+!u_`>UiGFC!;S;?}7v%%HCCZH-/V/09i6rcl!Iz%D/CTOncy');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
